# Statuspage.io Replicator

The aim of this project is to enable replication of status information for arbitrary services from 
[statuspage.io](https://www.statuspage.io/) to Discord.

If you have feature requests, want to contribute or have any questions, just write to [philipp@phimath.de](philipp@phimath.de).

## Prerequisites
* Have .NET Core 3.1 Runtime installed. The SDK is not required. See the [Microsoft Docs](https://docs.microsoft.com/en-us/dotnet/core/install/) 
for more information.
* Have a REST API endpoint sufficing the following requisites:
  1. Authorization is taken through a header, and the scheme is "guid"
  2. Supports `POST` to set the bot online
  3. Supports `DELETE` to set the bot offline
  4. Returns the seconds of the heartbeat interval on `POST` requests; must not be less than a second
* Have an active statuspage.io account, and the corresponding page ID and API key
* Have a valid Discord Bot account token (only if `MessageType` is other than `WEBHOOK`)

## Configuration

This is an example configuration, the values are explained in more detail after the configuration:

```json5
{
    "Bot": {
        "MessageType": "<Append OR Replace OR Edit>",
        "BotToken": "<Discord Bot Token>",
        "ProxyEndpoint": "https://api.example.com/route/to/discord/presence",
        "ProxyToken": "a secret token"
    },
    "Cache:Location": "/path/to/location.cache.json",
    "Embeds": {
        "Footer": "<custom footer>",
        "StatusHeader": "<custom header for system status>",
        "ScheduledHeader": "Scheduled Maintenance Events"
    },
    "Icons": {
        "<your first guild id>": {
            "EMPTY": "<icon id of :empty: in your first guild>",
            "GREEN": "<icon id of :green: in your first guild>",
            "YELLOW": "<icon id of :yellow: in your first guild>",
            "ORANGE": "<icon id of :orange: in your first guild>",
            "RED": "<icon id of :red: in your first guild>",
            "BLUE": "<icon id of :blue: in your first guild>"
        }
    },
    "Intervals": {
        "Statuspage": <polling time in seconds>
    },
    "Log": "/path/to/logfolder",
    "Logging": {
        "LogLevel": {
            "Replicator": "<Log level for the app's components>"
        }
    },
    "Modules": {
        "Components": true,
         "Shards": true,
         "Incidents": true,
         "Maintenance": true
    },
    "Statuspage": {
        "PageID": "<your page id>",
        "ApiKey": "<your page API key>"
    },
    "Webhooks": {
        "Components": [
            "https://discordapp.com/api/webhooks/<channel id>/<webhook token>"
        ],
        "Shards": [
            "https://discordapp.com/api/webhooks/<channel id>/<webhook token>"
        ],
        "Incidents": [
            "https://discordapp.com/api/webhooks/<channel id>/<webhook token>"
        ],
        "Maintenance": [
            "https://discordapp.com/api/webhooks/<channel id>/<webhook token>"
        ]
    }
}
```

### Detailed Configuration

This is a list of all available configuration values:

* ``Bot:MessageType``:``enum``

   **Append**: Just post the message to the target channel
   
   **Replace**: Delete all messages known to the application (i.e. previously posted and saved to the cache) in the
   target channel and then post the message
   
   **Edit**: Edit the last known message in the target channel, if present, or post a new one otherwise
   
   **Webhook**: Appends the messages to the target channel using webhooks (usually does not run into rate limits)
   
* ``Bot:ApiToken``:``string`` Token for your bot retrieved from the [Discord Application Portal](https://discordapp.com/developers/applications)

* ``Cache:Location``:``string`` Path to the cache JSON file
  
* ``Logging:LogLevel:Replicator``:``enum`` Setting the log level for all app services
  
  ``Logging:LogLevel:Replicator.Cache``:``enum`` Setting the log level for the caching service
  
  ``Logging:LogLevel:Replicator.Components``:``enum`` Setting the log level for the components publishing service
  
  ``Logging:LogLevel:Replicator.Incidents``:``enum`` Setting the log level for the incidents publishing service
  
  ``Logging:LogLevel:Replicator.Maintenance``:``enum`` Setting the log level for the maintenance publishing service
  
  ``Logging:LogLevel:Replicator.Shards``:``enum`` Setting the log level for the shards publishing service
  
  See [Microsoft Docs](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/logging/?view=aspnetcore-3.1#log-filtering)
  for the values you can provide here
  
