using System;
using System.IO;
using System.Text.Json;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

using NetEscapades.Extensions.Logging.RollingFile;

using Phimath.Discord.StatuspageReplicator.Models;
using Phimath.Discord.StatuspageReplicator.Services;
using Phimath.Discord.StatuspageReplicator.Services.DiscordPresence;
using Phimath.Discord.StatuspageReplicator.Services.Gateway;
using Phimath.Discord.StatuspageReplicator.Services.Messaging;
using Phimath.Discord.StatuspageReplicator.Services.REST;
using Phimath.Discord.StatuspageReplicator.Services.REST.DiscordImpl;
using Phimath.Discord.StatuspageReplicator.Services.REST.Impl;
using Phimath.Discord.StatuspageReplicator.Services.REST.StatuspageImpl;
using Phimath.Discord.StatuspageReplicator.Services.Target;
using Phimath.Discord.StatuspageReplicator.Services.Target.ExecutorImpl;

namespace Phimath.Discord.StatuspageReplicator
{
    public static class Program
    {
        internal static readonly JsonSerializerOptions NullableJsonOpts;

        static Program()
        {
            NullableJsonOpts = new JsonSerializerOptions();
            NullableJsonOpts.IgnoreNullValues = false;
        }

        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        private static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                       .UseWindowsService()
                       .ConfigureServices(
                           (hostContext, services) =>
                           {
                               var configuration = hostContext.Configuration;

                               var isService = hostContext.HostingEnvironment.IsProduction();

                               var loggingDirectory = configuration.GetValue<string>("Logging:Directory");

                               services.AddTransient(
                                   sp => sp.GetRequiredService<IConfiguration>()
                                           .GetSection("App:Gateway")
                                           .Get<BindableConfiguration>()
                               );

                               services.AddLogging(
                                   b =>
                                   {
                                       b.ClearProviders();
                                       if (!isService)
                                       {
                                           b.AddConsole();
                                       }

                                       if (isService || loggingDirectory != null)
                                       {
                                           b.AddFile(
                                               o =>
                                               {
                                                   o.LogDirectory = Path.GetDirectoryName(loggingDirectory);
                                                   o.Extension = ".log";
                                                   o.Periodicity = PeriodicityOptions.Daily;
                                                   o.FileName = "vanir-";
                                                   o.FileSizeLimit = null;
                                                   o.RetainedFileCountLimit = 7;
                                               }
                                           );
                                       }
                                   }
                               );

                               var messageType = MessageType.Webhook;
                               {
                                   var type = configuration.GetValue<string>("App:MessageType");
                                   if (type != null)
                                   {
                                       messageType = Enum.Parse<MessageType>(type, true);
                                   }
                               }

                               services.AddSingleton<ITargetFactory, TargetFactory>();

                               services.AddSingleton<DiscordGateway>();
                               services.AddSingleton<IDiscordApi, GatewayDiscordApi>();
                               services.AddHostedService<DiscordPresenceWorker>();

                               services.AddSingleton<IUpdateExecutor, ReplaceExecutor>();

                               services.AddSingleton<IStatuspageApi, StatuspageApi>();

                               // Used for inter-process communication
                               services.AddSingleton<Communicator>();

                               // Used for controlling when to send updates
                               services.AddSingleton<Cache>();

                               // Used for meta-data operations
                               services.AddSingleton<MetaService>();

                               // The worker to retrieve the updates
                               services.AddHostedService<StatuspageRetriever>();

                               void AddTarget<TEventArgs>() where TEventArgs : StatusEventArgs
                               {
                                   if (configuration.GetValue<bool?>(
                                                        $"Modules:{typeof(TEventArgs).Name.Replace("EventArgs", string.Empty)}"
                                                    )
                                                    .GetValueOrDefault(true))
                                   {
                                       services.AddHostedService<Updater<TEventArgs>>();
                                   }
                               }

                               // The status publishers
                               AddTarget<ComponentsEventArgs>();

                               // The shard status publishers
                               AddTarget<ShardsEventArgs>();

                               // The incident publishers
                               AddTarget<IncidentsEventArgs>();

                               // The maintenance publishers
                               AddTarget<MaintenanceEventArgs>();
                           }
                       );
        }
    }
}
