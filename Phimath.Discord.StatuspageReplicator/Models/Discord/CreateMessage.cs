using System.Text.Json.Serialization;

namespace Phimath.Discord.StatuspageReplicator.Models.Discord
{
    public struct CreateMessage
    {
        [JsonPropertyName("tts")]
        public bool TextToSpeech { get; set; }

        [JsonPropertyName("embed")]
        public Embed Embed { get; set; }
    }
}
