using System.Text.Json.Serialization;

namespace Phimath.Discord.StatuspageReplicator.Models.Discord
{
    public struct Channel
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("guild_id")]
        public string GuildId { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("type")]
        public int Type { get; set; }
    }
}
