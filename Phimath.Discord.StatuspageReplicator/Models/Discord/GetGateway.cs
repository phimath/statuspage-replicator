using System.Text.Json.Serialization;

namespace Phimath.Discord.StatuspageReplicator.Models.Discord
{
    public struct GetGateway
    {
        [JsonPropertyName("url")]
        public string Url { get; set; }

        [JsonPropertyName("shards")]
        public int Shards { get; set; }
    }
}
