using System.Text.Json.Serialization;

namespace Phimath.Discord.StatuspageReplicator.Models.Discord
{
    public sealed class CreateEmbedWebhook : CreateWebhook
    {
        [JsonPropertyName("embeds")]
        public Embed[] Embeds { get; set; }
    }
}
