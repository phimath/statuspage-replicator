using System.Text.Json.Serialization;

namespace Phimath.Discord.StatuspageReplicator.Models.Discord
{
    public abstract class CreateWebhook
    {
        [JsonPropertyName("username")]
        public string Username { get; set; }

        [JsonPropertyName("avatar_url")]
        public string AvatarUrl { get; set; }

        [JsonPropertyName("tts")]
        public bool TextToSpeech { get; set; } = false;
    }
}
