using System.Text.Json.Serialization;

namespace Phimath.Discord.StatuspageReplicator.Models.Discord
{
    public struct Message
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("channel_id")]
        public string ChannelId { get; set; }

        [JsonPropertyName("nonce")]
        public string Nonce { get; set; }

        [JsonPropertyName("embeds")]
        public Embed[] Embeds { get; set; }
    }
}
