using System.Text.Json.Serialization;

namespace Phimath.Discord.StatuspageReplicator.Models.Discord
{
    public struct EditMessage
    {
        [JsonPropertyName("embed")]
        public Embed Embed { get; set; }
    }
}
