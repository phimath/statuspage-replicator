using System.Text.Json.Serialization;

namespace Phimath.Discord.StatuspageReplicator.Models.Statuspage
{
    public struct IncidentUpdateComponent
    {
        [JsonPropertyName("code")]
        public string Id { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("old_status")]
        public string OldStatus { get; set; }

        [JsonPropertyName("new_status")]
        public string NewStatus { get; set; }
    }
}
