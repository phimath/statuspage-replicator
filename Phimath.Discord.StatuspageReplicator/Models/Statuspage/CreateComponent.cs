using System.Text.Json.Serialization;

namespace Phimath.Discord.StatuspageReplicator.Models.Statuspage
{
    public struct CreateComponent
    {
        [JsonPropertyName("group_id")]
        public string GroupId { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("description")]
        public string Description { get; set; }

        [JsonPropertyName("status")]
        public string Status { get; set; }
    }
}
