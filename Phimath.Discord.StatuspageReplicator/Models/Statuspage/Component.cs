using System;
using System.Diagnostics;
using System.Text.Json.Serialization;

namespace Phimath.Discord.StatuspageReplicator.Models.Statuspage
{
    [DebuggerDisplay("({Id}) {Name}")]
    public struct Component
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("group_id")]
        public string GroupId { get; set; }

        [JsonPropertyName("created_at")]
        public DateTime CreatedAt { get; set; }

        [JsonPropertyName("updated_at")]
        public DateTime UpdatedAt { get; set; }

        [JsonPropertyName("group")]
        public bool Group { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("description")]
        public string Description { get; set; }

        [JsonPropertyName("position")]
        public int Position { get; set; }

        [JsonPropertyName("status")]
        public string Status { get; set; }

        [JsonPropertyName("automation_email")]
        public string AutomationEmail { get; set; }

        public override bool Equals(object obj)
        {
            return obj is Component c && c.Id == Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
