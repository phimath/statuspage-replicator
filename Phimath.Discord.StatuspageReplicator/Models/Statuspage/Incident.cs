using System;
using System.Text.Json.Serialization;

using Phimath.Discord.StatuspageReplicator.Services;

namespace Phimath.Discord.StatuspageReplicator.Models.Statuspage
{
    public struct Incident
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("components")]
        public Component[] Components { get; set; }

        [JsonPropertyName("created_at")]
        public DateTime CreatedAt { get; set; }

        [JsonIgnore]
        internal Impact ImpactInternal
        {
            get => Enum.Parse<Impact>(Impact, true);
            set => Impact = value.ToString().ToLowerInvariant();
        }

        [JsonIgnore]
        internal IncidentType Type { get; set; }

        [JsonPropertyName("impact")]
        public string Impact { get; set; }

        [JsonPropertyName("impact_override")]
        public string ImpactOverride { get; set; }

        [JsonPropertyName("incident_updates")]
        public IncidentUpdate[] IncidentUpdates { get; set; }

        // [JsonPropertyName("metadata")] public object Metadata { get; set; }
        [JsonPropertyName("monitoring_at")]
        public DateTime? MonitoringAt { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("page_id")]
        public string PageId { get; set; }

        [JsonPropertyName("postmortem_body")]
        public string PostmortemBody { get; set; }

        [JsonPropertyName("postmortem_body_last_updated_at")]
        public DateTime? PostmortemBodyLastUpdatedAt { get; set; }

        [JsonPropertyName("postmortem_ignored")]
        public bool PostmortemIgnored { get; set; }

        [JsonPropertyName("postmortem_notified_subscribers")]
        public bool PostmortemNotifiedSubscribers { get; set; }

        [JsonPropertyName("postmortem_notified_twitter")]
        public bool PostmortemNotifiedTwitter { get; set; }

        [JsonPropertyName("postmortem_published_at")]
        public DateTime? PostmortemPublishedAt { get; set; }

        [JsonPropertyName("resolved_at")]
        public DateTime? ResolvedAt { get; set; }

        [JsonPropertyName("scheduled_auto_completed")]
        public bool ScheduledAutoCompleted { get; set; }

        [JsonPropertyName("scheduled_auto_in_progress")]
        public bool ScheduledAutoInProgress { get; set; }

        [JsonPropertyName("scheduled_for")]
        public DateTime? ScheduledFor { get; set; }

        [JsonPropertyName("scheduled_remind_prior")]
        public bool ScheduledRemindPrior { get; set; }

        [JsonPropertyName("scheduled_reminded_at")]
        public DateTime? ScheduledRemindedAt { get; set; }

        [JsonPropertyName("scheduled_until")]
        public DateTime? ScheduledUntil { get; set; }

        [JsonPropertyName("shortlink")]
        public string Link { get; set; }

        [JsonPropertyName("status")]
        public string Status { get; set; }

        [JsonPropertyName("updated_at")]
        public DateTime UpdatedAt { get; set; }
    }
}
