using System;
using System.Text.Json.Serialization;

namespace Phimath.Discord.StatuspageReplicator.Models.Statuspage
{
    public struct IncidentUpdate
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("incident_id")]
        public string IncidentId { get; set; }

        [JsonPropertyName("affected_components")]
        public IncidentUpdateComponent[] AffectedComponents { get; set; }

        [JsonPropertyName("body")]
        public string Body { get; set; }

        [JsonPropertyName("created_at")]
        public DateTime CreatedAt { get; set; }

        [JsonPropertyName("custom_tweet")]
        public string CustomTweet { get; set; }

        [JsonPropertyName("deliver_notifications")]
        public bool DeliverNotifications { get; set; }

        [JsonPropertyName("display_at")]
        public DateTime DisplayAt { get; set; }

        [JsonPropertyName("status")]
        public string Status { get; set; }

        [JsonPropertyName("tweet_id")]
        public ulong? TweetId { get; set; }

        [JsonPropertyName("twitter_updated_at")]
        public DateTime? TwitterUpdatedAt { get; set; }

        [JsonPropertyName("updated_at")]
        public DateTime UpdatedAt { get; set; }

        [JsonPropertyName("wants_twitter_update")]
        public bool WantsTwitterUpdate { get; set; }
    }
}
