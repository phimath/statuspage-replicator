namespace Phimath.Discord.StatuspageReplicator.Models
{
    public enum MessageType
    {
        /// <summary>
        ///     Just append the message to the end of the channel using the supplied webhook.
        /// </summary>
        Webhook,

        /// <summary>
        ///     Just append the message to the end of the channel using the Bot account.
        /// </summary>
        Append,

        /// <summary>
        ///     Delete all previous messages known to the bot in this channel, then post the new message.
        /// </summary>
        Replace,

        /// <summary>
        ///     Edit any existing message, or post a new one of none exists.
        /// </summary>
        Edit,
    }
}
