using System;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

using Phimath.Telemetry;

namespace Phimath.Discord.StatuspageReplicator.Services
{
    public class TelemetryService : IHostedService
    {
        private readonly ILogger _logger;
        private readonly IAppTelemetry _telemetry;
        private Guid _installationId;

        public TelemetryService(ILogger<TelemetryService> logger)
        {
            _logger = logger;
            _telemetry = Telemetry.Telemetry.Initialize();
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            _installationId = _telemetry.GetOrCreateInstallationId();
            _logger.LogInformation($"App ID is {_telemetry.AppId}");
            _logger.LogInformation($"Installation ID is {_installationId}");

            _telemetry.SubmitTelemetry(new VersionTelemetryData());
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
        }

        private class VersionTelemetryData : ISerializableTelemetryData
        {
            private readonly Version _version;

            public VersionTelemetryData()
            {
                _version = Assembly.GetExecutingAssembly().GetName().Version;
            }

            public string Serialize()
            {
                return "{\"version\": \"" + _version.ToString(3) + "\"}";
            }
        }
    }
}
