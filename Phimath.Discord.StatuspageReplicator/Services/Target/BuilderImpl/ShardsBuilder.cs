using Phimath.Discord.StatuspageReplicator.Models.Discord;
using Phimath.Discord.StatuspageReplicator.Services.Messaging;
using Phimath.Discord.StatuspageReplicator.Services.REST;

namespace Phimath.Discord.StatuspageReplicator.Services.Target.BuilderImpl
{
    internal class ShardsBuilder : IContentBuilder<ShardsEventArgs>
    {
        private readonly MetaService _meta;

        public ShardsBuilder(MetaService meta)
        {
            _meta = meta;
        }

        Embed[] IContentBuilder<ShardsEventArgs>.BuildContent(ShardsEventArgs @event, IGuildChannel target)
        {
            return new[] { _meta.BuildShardStatus(@event.MaxImpact, @event.Components, target.GuildId) };
        }
    }
}
