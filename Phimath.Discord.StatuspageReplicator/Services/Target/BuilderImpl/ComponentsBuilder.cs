using Phimath.Discord.StatuspageReplicator.Models.Discord;
using Phimath.Discord.StatuspageReplicator.Services.Messaging;
using Phimath.Discord.StatuspageReplicator.Services.REST;

namespace Phimath.Discord.StatuspageReplicator.Services.Target.BuilderImpl
{
    internal class ComponentsBuilder : IContentBuilder<ComponentsEventArgs>
    {
        private readonly MetaService _meta;

        public ComponentsBuilder(MetaService meta)
        {
            _meta = meta;
        }

        Embed[] IContentBuilder<ComponentsEventArgs>.BuildContent(ComponentsEventArgs @event, IGuildChannel target)
        {
            return new[]
            {
                _meta.BuildStatus(@event.MaxImpact, @event.Components, target.GuildId),
                _meta.BuildScheduledMaintenance(@event.ScheduledMaintenance),
            };
        }
    }
}
