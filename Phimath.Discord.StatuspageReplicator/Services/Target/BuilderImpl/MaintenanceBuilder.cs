using Phimath.Discord.StatuspageReplicator.Models.Discord;
using Phimath.Discord.StatuspageReplicator.Services.Messaging;
using Phimath.Discord.StatuspageReplicator.Services.REST;

namespace Phimath.Discord.StatuspageReplicator.Services.Target.BuilderImpl
{
    internal class MaintenanceBuilder : IContentBuilder<MaintenanceEventArgs>
    {
        private readonly MetaService _meta;
        private Embed[] _embeds;

        public MaintenanceBuilder(MetaService meta)
        {
            _meta = meta;
        }

        void IContentBuilder<MaintenanceEventArgs>.PreBuild(MaintenanceEventArgs @event)
        {
            _embeds = new[]
            {
                _meta.BuildActiveMaintenance(@event.ActiveMaintenance),
                _meta.BuildScheduledMaintenance(@event.ScheduledMaintenance),
            };
        }

        Embed[] IContentBuilder<MaintenanceEventArgs>.BuildContent(MaintenanceEventArgs @event, IGuildChannel target)
        {
            return _embeds;
        }

        void IContentBuilder<MaintenanceEventArgs>.PostBuild()
        {
            _embeds = null;
        }
    }
}
