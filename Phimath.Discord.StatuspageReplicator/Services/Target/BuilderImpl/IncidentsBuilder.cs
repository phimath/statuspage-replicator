using Phimath.Discord.StatuspageReplicator.Models.Discord;
using Phimath.Discord.StatuspageReplicator.Services.Messaging;
using Phimath.Discord.StatuspageReplicator.Services.REST;

namespace Phimath.Discord.StatuspageReplicator.Services.Target.BuilderImpl
{
    internal class IncidentsBuilder : IContentBuilder<IncidentsEventArgs>
    {
        private readonly MetaService _meta;
        private Embed[] _content;

        public IncidentsBuilder(MetaService meta)
        {
            _meta = meta;
        }

        void IContentBuilder<IncidentsEventArgs>.PreBuild(IncidentsEventArgs @event)
        {
            _content = new[] { _meta.BuildIncident(@event.Incidents, @event.MaxImpact) };
        }

        Embed[] IContentBuilder<IncidentsEventArgs>.BuildContent(IncidentsEventArgs @event, IGuildChannel target)
        {
            return _content;
        }

        void IContentBuilder<IncidentsEventArgs>.PostBuild()
        {
            _content = null;
        }
    }
}
