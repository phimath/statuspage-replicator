using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Microsoft.Extensions.Logging;

using Phimath.Discord.StatuspageReplicator.Internal;
using Phimath.Discord.StatuspageReplicator.Models.Discord;
using Phimath.Discord.StatuspageReplicator.Services.Messaging;
using Phimath.Discord.StatuspageReplicator.Services.REST;

namespace Phimath.Discord.StatuspageReplicator.Services.Target
{
    public interface IContentBuilder<in T> where T : StatusEventArgs
    {
        public async Task Execute(
            IUpdateExecutor executor,
            T @event,
            IEnumerable<IGuildChannel> targets,
            ILogger logger
        )
        {
            var targetType = typeof(T).GetTargetType();

            if (!targetType.HasValue)
            {
                throw new InvalidOperationException();
            }

            PreBuild(@event);

            foreach (var target in targets)
            {
                var targetData = BuildContent(@event, target);
                await executor.Execute(target, targetData, logger);
            }

            PostBuild();
        }

        internal void PreBuild(T @event)
        {
        }

        internal void PostBuild()
        {
        }

        internal Embed[] BuildContent(T @event, IGuildChannel target);
    }
}
