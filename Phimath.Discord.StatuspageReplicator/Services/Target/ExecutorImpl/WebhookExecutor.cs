using System;
using System.Threading.Tasks;

using Microsoft.Extensions.Logging;

using Phimath.Discord.StatuspageReplicator.Models.Discord;
using Phimath.Discord.StatuspageReplicator.Services.REST;

namespace Phimath.Discord.StatuspageReplicator.Services.Target.ExecutorImpl
{
    public class WebhookExecutor : IUpdateExecutor
    {
        public async Task Execute(IGuildChannel target, Embed[] data, ILogger logger)
        {
            if (target is not IDiscordWebhook webhook)
            {
                throw new InvalidOperationException("Input target needs to be a webhook");
            }

            var message = new CreateEmbedWebhook { Embeds = data };

            await webhook.Execute(message);
        }
    }
}
