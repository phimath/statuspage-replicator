using System.Threading.Tasks;

using Microsoft.Extensions.Logging;

using Phimath.Discord.StatuspageReplicator.Extensions;
using Phimath.Discord.StatuspageReplicator.Models.Discord;
using Phimath.Discord.StatuspageReplicator.Services.REST;

namespace Phimath.Discord.StatuspageReplicator.Services.Target.ExecutorImpl
{
    public class EditExecutor : IUpdateExecutor
    {
        private readonly IDiscordApi _api;
        private readonly Cache _cache;

        public EditExecutor(IDiscordApi api, Cache cache)
        {
            _api = api;
            _cache = cache;
        }

        public async Task Execute(IGuildChannel target, Embed[] data, ILogger logger)
        {
            foreach (var datum in data)
            {
                var targetType = datum._targetType;
                logger.LogInformation("Finding last message");
                var cache = _cache.GetGuildCache(target.ChannelId);
                var id = cache[targetType];
                if (!id.HasValue)
                {
                    var existing = await _api.FindLastMessage(target.ChannelId, targetType);
                    if (existing.HasValue)
                    {
                        logger.LogInformation("Deleting message");
                        id = existing.Value.Id.ToULong();
                    }
                    else
                    {
                        logger.LogInformation("No existing found");
                    }
                }

                ulong newId;
                if (id.HasValue)
                {
                    logger.LogInformation($"Editing message at {target.GuildId}/{target.ChannelId}");
                    var editedMessage = await _api.EditMessage(
                        new EditMessage { Embed = datum },
                        target.ChannelId,
                        id.Value.ToString()
                    );
                    newId = editedMessage.Id.ToULong();
                }
                else
                {
                    logger.LogInformation($"Sending message to {target.GuildId}/{target.ChannelId}");
                    var newMessage = await _api.CreateMessage(new CreateMessage { Embed = datum }, target);
                    newId = newMessage.Id.ToULong();
                }

                // Update cache
                cache[targetType] = newId;
                _cache.Dump();
            }
        }
    }
}
