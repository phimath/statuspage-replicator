using System.Threading.Tasks;

using Microsoft.Extensions.Logging;

using Phimath.Discord.StatuspageReplicator.Models.Discord;
using Phimath.Discord.StatuspageReplicator.Services.REST;

namespace Phimath.Discord.StatuspageReplicator.Services.Target.ExecutorImpl
{
    public class AppendExecutor : IUpdateExecutor
    {
        private readonly IDiscordApi _api;

        public AppendExecutor(IDiscordApi api)
        {
            _api = api;
        }

        public async Task Execute(IGuildChannel target, Embed[] data, ILogger logger)
        {
            foreach (var datum in data)
            {
                await _api.CreateMessage(new CreateMessage { Embed = datum }, target);
            }
        }
    }
}
