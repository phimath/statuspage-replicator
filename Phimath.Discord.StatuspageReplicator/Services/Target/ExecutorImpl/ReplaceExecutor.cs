using System.Threading.Tasks;

using Microsoft.Extensions.Logging;

using Phimath.Discord.StatuspageReplicator.Extensions;
using Phimath.Discord.StatuspageReplicator.Models.Discord;
using Phimath.Discord.StatuspageReplicator.Services.REST;

namespace Phimath.Discord.StatuspageReplicator.Services.Target.ExecutorImpl
{
    public class ReplaceExecutor : IUpdateExecutor
    {
        private readonly IDiscordApi _api;
        private readonly Cache _cache;

        public ReplaceExecutor(IDiscordApi api, Cache cache)
        {
            _api = api;
            _cache = cache;
        }

        public async Task Execute(IGuildChannel target, Embed[] data, ILogger logger)
        {
            foreach (var datum in data)
            {
                var targetType = datum._targetType;
                logger.LogInformation($"Finding last message for target {targetType}");
                var cache = _cache.GetGuildCache(target.ChannelId);
                var id = cache[targetType];
                if (!id.HasValue)
                {
                    var existing = await _api.FindLastMessage(target.ChannelId, targetType);
                    if (existing.HasValue)
                    {
                        logger.LogInformation("Deleting message");
                        id = existing.Value.Id.ToULong();
                    }
                    else
                    {
                        logger.LogInformation("No existing found");
                    }
                }

                if (id.HasValue)
                {
                    await _api.DeleteMessage(target.ChannelId, id.Value.ToString());
                }

                logger.LogInformation($"Sending message to {target.GuildId}/{target.ChannelId}");
                var newMessage = await _api.CreateMessage(new CreateMessage { Embed = datum }, target);

                // Update cache
                cache[targetType] = newMessage.Id.ToULong();
                _cache.Dump();
            }
        }
    }
}
