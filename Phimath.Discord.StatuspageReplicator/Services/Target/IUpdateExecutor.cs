using System.Threading.Tasks;

using Microsoft.Extensions.Logging;

using Phimath.Discord.StatuspageReplicator.Models.Discord;
using Phimath.Discord.StatuspageReplicator.Services.REST;

namespace Phimath.Discord.StatuspageReplicator.Services.Target
{
    public interface IUpdateExecutor
    {
        Task Execute(IGuildChannel target, Embed[] data, ILogger logger);
    }
}
