using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

using Phimath.Discord.StatuspageReplicator.Services.Messaging;
using Phimath.Discord.StatuspageReplicator.Services.REST;
using Phimath.Discord.StatuspageReplicator.Services.Target.BuilderImpl;

namespace Phimath.Discord.StatuspageReplicator.Services.Target
{
    internal sealed class Updater<T> : IHostedService, IDisposable where T : StatusEventArgs
    {
        private readonly IContentBuilder<T> _builder;
        private readonly IUpdateExecutor _executor;
        private readonly ILogger _logger;
        private readonly IReadOnlyList<IGuildChannel> _targets;
        private readonly string _typeName;

        public Updater(
            ITargetFactory targetFactory,
            ILoggerFactory lf,
            IConfiguration configuration,
            MetaService meta,
            Communicator communicator,
            IUpdateExecutor executor
        )
        {
            _typeName = typeof(T).Name.Replace("EventArgs", string.Empty);
            _logger = lf.CreateLogger($"Replicator.{_typeName}");
            _executor = executor;
            switch (typeof(T).Name)
            {
                case nameof(ComponentsEventArgs):
                    communicator.StatusPublished += Unsafe.As<Updater<ComponentsEventArgs>>(this).Execute;
                    _builder = (IContentBuilder<T>)new ComponentsBuilder(meta);
                    break;
                case nameof(IncidentsEventArgs):
                    communicator.IncidentsPublished += Unsafe.As<Updater<IncidentsEventArgs>>(this).Execute;
                    _builder = (IContentBuilder<T>)new IncidentsBuilder(meta);
                    break;
                case nameof(MaintenanceEventArgs):
                    communicator.MaintenancePublished += Unsafe.As<Updater<MaintenanceEventArgs>>(this).Execute;
                    _builder = (IContentBuilder<T>)new MaintenanceBuilder(meta);
                    break;
                case nameof(ShardsEventArgs):
                    communicator.ShardStatusPublished += Unsafe.As<Updater<ShardsEventArgs>>(this).Execute;
                    _builder = (IContentBuilder<T>)new ShardsBuilder(meta);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(T), "Unknown event args");
            }

            var configurationTargets = configuration.GetSection($"App:Targets:{_typeName}").Get<string[]>();
            if (configurationTargets != null)
            {
                var targets = new List<IGuildChannel>(configurationTargets.Length);

                foreach (var targetUri in configurationTargets)
                {
                    var guildChannel = targetFactory.Create(targetUri, _typeName, _logger);
                    targets.Add(guildChannel);
                }

                _targets = targets;
            }
            else
            {
                _targets = new List<IGuildChannel>();
            }
        }

        public void Dispose()
        {
            foreach (var target in _targets)
            {
                if (target is IDisposable disposable)
                {
                    disposable.Dispose();
                }
            }
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        private async void Execute(T @event)
        {
            try
            {
                await _builder.Execute(_executor, @event, _targets, _logger);
            }
            catch (Exception e)
            {
                _logger.LogCritical(e, $"Error publishing {_typeName}");
            }
        }

        ~Updater()
        {
            Dispose();
        }
    }
}
