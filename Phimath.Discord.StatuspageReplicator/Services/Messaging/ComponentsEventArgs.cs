// Copyright 2021 (c) phimath.
// All rights reserved if not stated otherwise or licensed under one or more agreements.
// If applicable, license agreements can be found in the top most level of the source repository.

using System.Collections.Generic;
using System.Linq;

using Phimath.Discord.StatuspageReplicator.Internal;
using Phimath.Discord.StatuspageReplicator.Models.Statuspage;

namespace Phimath.Discord.StatuspageReplicator.Services.Messaging
{
    [ForType(TargetType.Components)]
    public sealed class ComponentsEventArgs : StatusEventArgs
    {
        internal ComponentsEventArgs(
            Impact maxImpact,
            Component[] components,
            Dictionary<string, List<Incident>> componentsToIncident,
            Dictionary<string, List<Incident>> componentsToMaintenance,
            Incident[] scheduledMaintenance
        )
        {
            MaxImpact = maxImpact;
            ScheduledMaintenance = scheduledMaintenance;
            Components = components.Select(c => (c, componentsToIncident[c.Id], componentsToMaintenance[c.Id]))
                                   .ToArray();
        }

        internal Impact MaxImpact { get; }

        internal (Component component, List<Incident> openIncidents, List<Incident> underMaintenance)[] Components
        {
            get;
        }

        internal Incident[] ScheduledMaintenance { get; }
    }
}
