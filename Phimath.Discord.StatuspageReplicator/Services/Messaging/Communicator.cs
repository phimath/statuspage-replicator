using System;
using System.Collections.Generic;

using Phimath.Discord.StatuspageReplicator.Models.Statuspage;

namespace Phimath.Discord.StatuspageReplicator.Services.Messaging
{
    public class Communicator
    {
        internal event Action<ComponentsEventArgs> StatusPublished;

        internal event Action<ShardsEventArgs> ShardStatusPublished;

        internal event Action<IncidentsEventArgs> IncidentsPublished;

        internal event Action<MaintenanceEventArgs> MaintenancePublished;

        internal void OnStatus(
            Impact maxImpact,
            Component[] components,
            Dictionary<string, List<Incident>> componentsToIncident,
            Dictionary<string, List<Incident>> componentsToMaintenance,
            Incident[] scheduledMaintenance
        )
        {
            StatusPublished?.Invoke(
                new ComponentsEventArgs(
                    maxImpact,
                    components,
                    componentsToIncident,
                    componentsToMaintenance,
                    scheduledMaintenance
                )
            );
        }

        internal void OnShardStatus(
            Component[] shards,
            Dictionary<string, List<Incident>> componentsToIncident,
            Dictionary<string, List<Incident>> componentsToMaintenance,
            Incident[] scheduledMaintenance
        )
        {
            ShardStatusPublished?.Invoke(
                new ShardsEventArgs(shards, componentsToIncident, componentsToMaintenance, scheduledMaintenance)
            );
        }

        internal void OnIncidents(Incident[] incidents, Impact maxImpact)
        {
            IncidentsPublished?.Invoke(new IncidentsEventArgs(incidents, maxImpact));
        }

        internal void OnMaintenance(Incident[] activeMaintenance, Incident[] scheduledMaintenance)
        {
            MaintenancePublished?.Invoke(new MaintenanceEventArgs(activeMaintenance, scheduledMaintenance));
        }
    }
}
