// Copyright 2021 (c) phimath.
// All rights reserved if not stated otherwise or licensed under one or more agreements.
// If applicable, license agreements can be found in the top most level of the source repository.

using Phimath.Discord.StatuspageReplicator.Internal;
using Phimath.Discord.StatuspageReplicator.Models.Statuspage;

namespace Phimath.Discord.StatuspageReplicator.Services.Messaging
{
    [ForType(TargetType.Maintenance)]
    public sealed class MaintenanceEventArgs : StatusEventArgs
    {
        internal MaintenanceEventArgs(Incident[] activeMaintenance, Incident[] scheduledMaintenance)
        {
            ActiveMaintenance = activeMaintenance;
            ScheduledMaintenance = scheduledMaintenance;
        }

        internal Incident[] ActiveMaintenance { get; }

        internal Incident[] ScheduledMaintenance { get; }
    }
}
