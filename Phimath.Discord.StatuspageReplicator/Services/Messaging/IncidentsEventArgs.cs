// Copyright 2021 (c) phimath.
// All rights reserved if not stated otherwise or licensed under one or more agreements.
// If applicable, license agreements can be found in the top most level of the source repository.

using Phimath.Discord.StatuspageReplicator.Internal;
using Phimath.Discord.StatuspageReplicator.Models.Statuspage;

namespace Phimath.Discord.StatuspageReplicator.Services.Messaging
{
    [ForType(TargetType.Incidents)]
    public sealed class IncidentsEventArgs : StatusEventArgs
    {
        internal IncidentsEventArgs(Incident[] incidents, Impact maxImpact)
        {
            Incidents = incidents;
            MaxImpact = maxImpact;
        }

        internal Incident[] Incidents { get; }

        internal Impact MaxImpact { get; }
    }
}
