// Copyright 2021 (c) phimath.
// All rights reserved if not stated otherwise or licensed under one or more agreements.
// If applicable, license agreements can be found in the top most level of the source repository.

using System;
using System.Collections.Generic;
using System.Linq;

using Phimath.Discord.StatuspageReplicator.Internal;
using Phimath.Discord.StatuspageReplicator.Models.Statuspage;

namespace Phimath.Discord.StatuspageReplicator.Services.Messaging
{
    [ForType(TargetType.Shards)]
    public sealed class ShardsEventArgs : StatusEventArgs
    {
        internal ShardsEventArgs(
            Component[] components,
            Dictionary<string, List<Incident>> componentsToIncident,
            Dictionary<string, List<Incident>> componentsToMaintenance,
            Incident[] scheduledMaintenance
        )
        {
            ScheduledMaintenance = scheduledMaintenance;
            Components = components.Select(c => (c, componentsToIncident[c.Id], componentsToMaintenance[c.Id]))
                                   .ToArray();

            var incidentsMaxImpact = Components.Any()
                ? Components.Max(c => c.openIncidents.Any() ? c.openIncidents.Max(i => (byte)i.ImpactInternal) : 0)
                : 0;
            var maintenanceMaxImpact = Components.Any()
                ? Components.Max(
                    c => c.underMaintenance.Any() ? c.underMaintenance.Max(i => (byte)i.ImpactInternal) : 0
                )
                : 0;
            MaxImpact = (Impact)Math.Max(incidentsMaxImpact, maintenanceMaxImpact);
        }

        internal Impact MaxImpact { get; }

        internal (Component component, List<Incident> openIncidents, List<Incident> underMaintenance)[] Components
        {
            get;
        }

        internal Incident[] ScheduledMaintenance { get; }
    }
}
