using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

using Phimath.Discord.StatuspageReplicator.Services.Gateway;

namespace Phimath.Discord.StatuspageReplicator.Services.DiscordPresence
{
    public class DiscordPresenceWorker : IHostedService
    {
        private readonly ILogger _logger;
        private readonly string _version;
        private readonly HttpClient _wc;
        private readonly CancellationTokenSource _workerCancel = new();

        public DiscordPresenceWorker(ILogger<DiscordPresenceWorker> logger, BindableConfiguration config)
        {
            _version = Assembly.GetExecutingAssembly().GetName().Version!.ToString(3);

            var proxyEndpoint = config.GetPresenceEndpointUri();

            _logger = logger;
            _logger.LogInformation($"Initializing Gateway with proxy URL {proxyEndpoint}");

            _wc = new HttpClient
            {
                BaseAddress = proxyEndpoint,
                DefaultRequestHeaders =
                {
                    Authorization = new AuthenticationHeaderValue("GUID", config.Authorization),
                },
            };
        }

        private CancellationToken WorkerToken => _workerCancel.Token;

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Connecting Gateway");

            Task.Run(Worker, WorkerToken);

            return Task.CompletedTask;
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Shutting down");

            try
            {
                await _wc.DeleteAsync(string.Empty, cancellationToken);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error setting discord presence offline");
            }
        }

        private async void Worker()
        {
            while (!WorkerToken.IsCancellationRequested)
            {
                var seconds = 0;

                try
                {
                    var response = await _wc.SendAsync(
                        new HttpRequestMessage(HttpMethod.Post, string.Empty)
                        {
                            Content = new StringContent(_version, Encoding.UTF8),
                        },
                        WorkerToken
                    );
                    if (!response.IsSuccessStatusCode)
                    {
                        continue;
                    }

                    var responseString = await response.Content.ReadAsStringAsync();
                    if (!int.TryParse(responseString, out seconds))
                    {
                    }
                }
                catch (TaskCanceledException)
                {
                    StopAsync(CancellationToken.None).GetAwaiter().GetResult();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Error setting discord presence");
                }
                finally
                {
                    await Task.Delay(TimeSpan.FromMilliseconds(Math.Max((seconds - 1) * 1000, 500)), WorkerToken);
                }
            }
        }
    }
}
