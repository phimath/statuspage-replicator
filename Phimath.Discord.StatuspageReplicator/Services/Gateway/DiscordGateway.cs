using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading.Tasks;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using Phimath.Discord.StatuspageReplicator.Internal;
using Phimath.Discord.StatuspageReplicator.Services.REST;
using Phimath.Statuspage.Replicator.Internal;

namespace Phimath.Discord.StatuspageReplicator.Services.Gateway
{
    public class DiscordGateway : IApi
    {
        private readonly IDisposable _callback;
        protected readonly ILogger _logger;

        private readonly IServiceProvider _sp;

        private IApi _gateway;

        public DiscordGateway(IServiceProvider sp, ILogger<DiscordGateway> logger)
            : this(sp, (ILogger)logger)
        {
        }

        protected DiscordGateway(IServiceProvider sp, ILogger logger)
        {
            _sp = sp;
            _logger = logger;

            _callback = sp.GetRequiredService<IConfiguration>()
                          .GetReloadToken()
                          .RegisterChangeCallback(OnConfigurationReload, null);
            OnConfigurationReload(null);
        }

        private BindableConfiguration Configuration => _sp.GetRequiredService<BindableConfiguration>();

        public void Dispose()
        {
            _callback.Dispose();
            _gateway.Dispose();
        }

        public async Task Post(HttpContent content, string relPath = "", Action<HttpResponseMessage> consumer = null)
        {
            await _gateway.Post(content, relPath, consumer);
        }

        public async Task<T> Post<T>(
            HttpContent content,
            string relPath = "",
            Action<HttpResponseMessage> consumer = null
        )
        {
            return await _gateway.Post<T>(content, relPath, consumer);
        }

        public async Task<T> Get<T>(string relPath = "", Action<HttpResponseMessage> consumer = null)
        {
            return await _gateway.Get<T>(relPath, consumer);
        }

        public async Task Patch(HttpContent content, string relPath = "", Action<HttpResponseMessage> consumer = null)
        {
            await _gateway.Patch(content, relPath, consumer);
        }

        public async Task<T> Patch<T>(
            HttpContent content,
            string relPath = "",
            Action<HttpResponseMessage> consumer = null
        )
        {
            return await _gateway.Patch<T>(content, relPath, consumer);
        }

        public async Task Delete(string relPath = "", Action<HttpResponseMessage> consumer = null)
        {
            await _gateway.Delete(relPath, consumer);
        }

        public async Task<T> Delete<T>(string relPath = "", Action<HttpResponseMessage> consumer = null)
        {
            return await _gateway.Delete<T>(relPath, consumer);
        }

        private void OnConfigurationReload(object _)
        {
            var uri = Configuration.GetDataEndpointUri().ToString();
            _logger.LogInformation("From configuration, using ");
            _gateway = new InterceptingRestApi(_sp.GetRequiredService<IConfiguration>(), _logger, uri);
        }

        private sealed class InterceptingRestApi : AuthorizedApi
        {
            internal InterceptingRestApi(IConfiguration configuration, ILogger logger, string baseUrl)
                : base(configuration, logger, baseUrl)
            {
            }

            protected override async Task<T> WithClient<T>(
                string methodName,
                Func<HttpClient, Uri, Task<HttpResponseMessage>> action,
                string relPath = "",
                Action<HttpResponseMessage> consumer = null
            )
            {
                try
                {
                    var combinedUri = new Uri(_client.BaseAddress, $"?method={methodName}&path={relPath}");
                    var response = await action(_client, combinedUri);
                    consumer?.Invoke(response);

                    var responseMessage = await response.Content.ReadAsStringAsync();
                    if (response.IsSuccessStatusCode)
                    {
                        _logger.LogDebug($"{methodName} done ({combinedUri})");
                        if (response.StatusCode == HttpStatusCode.NoContent)
                        {
                            return default;
                        }
                    }
                    else if (response.StatusCode == HttpStatusCode.TooManyRequests)
                    {
                        throw new RateLimitedException(response, responseMessage);
                    }
                    else if (response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        throw new ForbiddenException();
                    }
                    else
                    {
                        throw new Exception(responseMessage);
                    }

                    if (typeof(T) == typeof(rvoid))
                    {
                        return default;
                    }

                    try
                    {
                        return JsonSerializer.Deserialize<T>(responseMessage);
                    }
                    catch (JsonException e)
                    {
                        throw new ApiJsonException(e, responseMessage);
                    }
                }
                catch (Exception e)
                {
                    _logger.LogError($"{methodName} resulted in error: {e.Message}");
                    return default;
                }
            }

            protected internal override AuthenticationHeaderValue GetAuthorization(IConfiguration configuration)
            {
                return new("guid", configuration["App:Gateway:Authorization"]);
            }
        }
    }
}
