using System;

namespace Phimath.Discord.StatuspageReplicator.Services.Gateway
{
    public sealed class BindableConfiguration
    {
        public string BaseUri { get; set; }

        public string PresenceEndpoint { get; set; }

        public string DataEndpoint { get; set; }

        public string Authorization { get; set; }
    }

    public static class BindableConfigurationExtensions
    {
        public static Uri GetDataEndpointUri(this BindableConfiguration obj)
        {
            return new(new Uri(obj.BaseUri, UriKind.Absolute), obj.DataEndpoint);
        }

        public static Uri GetPresenceEndpointUri(this BindableConfiguration obj)
        {
            return new(new Uri(obj.BaseUri, UriKind.Absolute), obj.PresenceEndpoint);
        }
    }
}
