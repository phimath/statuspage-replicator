// Copyright 2021 (c) phimath.
// All rights reserved if not stated otherwise or licensed under one or more agreements.
// If applicable, license agreements can be found in the top most level of the source repository.

namespace Phimath.Discord.StatuspageReplicator.Services
{
    internal enum Impact : byte
    {
        None = 0,
        Maintenance = 1,
        Minor = 2,
        Major = 3,
        Critical = 4,
    }
}
