using Microsoft.Extensions.Logging;

namespace Phimath.Discord.StatuspageReplicator.Services.REST
{
    public interface ITargetFactory
    {
        IGuildChannel Create(string targetUri, string targetName, ILogger logger);
    }
}
