namespace Phimath.Discord.StatuspageReplicator.Services.REST.Impl
{
    public class GuildChannel : IGuildChannel
    {
        public GuildChannel(string guild, string channel, string name, bool canCrossPost)
        {
            GuildId = guild;
            ChannelId = channel;
            Name = name;
            CanCrossPost = canCrossPost;
        }

        public string GuildId { get; }

        public string ChannelId { get; }

        public string Name { get; }

        public bool CanCrossPost { get; }
    }
}
