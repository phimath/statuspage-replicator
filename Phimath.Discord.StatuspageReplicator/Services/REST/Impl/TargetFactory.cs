using System;

using Microsoft.Extensions.Logging;

using Phimath.Discord.StatuspageReplicator.Models.Discord;
using Phimath.Discord.StatuspageReplicator.Services.REST.DiscordWebhookImpl;
using Phimath.Discord.StatuspageReplicator.Services.REST.GenericImpl;

namespace Phimath.Discord.StatuspageReplicator.Services.REST.Impl
{
    public class TargetFactory : ITargetFactory
    {
        private const string WebhookFormatString = "https://discordapp.com/api/webhooks{0}";

        private readonly IDiscordApi _discordApi;
        private readonly ILogger _logger;

        public TargetFactory(IDiscordApi discordApi, ILogger<TargetFactory> logger)
        {
            _discordApi = discordApi;
            _logger = logger;
        }

        public IGuildChannel Create(string targetUri, string targetName, ILogger logger)
        {
            _logger.LogInformation($"Creating target `{targetName}` for {targetUri}");

            var uri = new Uri(targetUri);
            switch (uri.Scheme.ToUpperInvariant())
            {
                case "DISCORD":
                    switch (uri.Authority.ToUpperInvariant())
                    {
                        case "CHANNELS":
                            return GuildChannel(uri.Segments[1], targetName, logger);
                        case "WEBHOOKS":
                            return Webhook(string.Format(WebhookFormatString, uri.AbsolutePath), targetName, logger);
                        default:
                            throw new ArgumentException(
                                $"Unknown discord authority {uri.Authority}",
                                nameof(targetUri)
                            );
                    }

                case "HTTP":
                    throw new InvalidOperationException("Unencrypted connections are not supported (use https://)");
                case "HTTPS":
                    return Webhook(targetUri, targetName, logger);
                default:
                    throw new ArgumentOutOfRangeException(nameof(targetUri), $"Unknown target scheme {uri.Scheme}");
            }
        }

        private IGuildChannel GuildChannel(string channelId, string targetName, ILogger logger)
        {
            var channelInfo = _discordApi.GetChannel(channelId).GetAwaiter().GetResult();

            return new GuildChannel(channelInfo.GuildId, channelId, channelInfo.Name, channelInfo.Type == 5);
        }

        private IDiscordWebhook Webhook(string targetUri, string targetName, ILogger logger)
        {
            using var api = new RestApi(targetUri, logger);
            var webhookInfo = api.Get<Webhook>().GetAwaiter().GetResult();

            return new DiscordWebhook(
                targetUri,
                targetName,
                webhookInfo.GuildId,
                webhookInfo.ChannelId,
                webhookInfo.Name,
                logger
            );
        }
    }
}
