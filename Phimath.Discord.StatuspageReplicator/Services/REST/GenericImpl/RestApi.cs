using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading.Tasks;

using Microsoft.Extensions.Logging;

using Phimath.Discord.StatuspageReplicator.Internal;
using Phimath.Statuspage.Replicator.Internal;

namespace Phimath.Discord.StatuspageReplicator.Services.REST.GenericImpl
{
    public class RestApi : IApi
    {
        protected readonly HttpClient _client;
        protected readonly ILogger _logger;

        public RestApi(string baseUrl, ILogger<RestApi> logger)
            : this(baseUrl, (ILogger)logger)
        {
        }

        protected internal RestApi(string baseUrl, ILogger logger)
        {
            _client = new HttpClient
            {
                BaseAddress = new Uri(baseUrl, UriKind.Absolute),
                DefaultRequestHeaders =
                {
                    UserAgent =
                    {
                        new ProductInfoHeaderValue(new ProductHeaderValue("StatusBot")),
                        new ProductInfoHeaderValue("(https://www.phimath.de, v1.0.0)"),
                    },
                    Accept = { MediaTypeWithQualityHeaderValue.Parse("application/json") },
                },
            };
            _logger = logger;
        }

        public async Task<T> Get<T>(string relPath = "", Action<HttpResponseMessage> consumer = null)
        {
            return await WithClient<T>(
                nameof(Get),
                async (client, uri) => await client.GetAsync(uri),
                relPath,
                consumer
            );
        }

        public async Task Post(HttpContent content, string relPath = "", Action<HttpResponseMessage> consumer = null)
        {
            await Post<rvoid>(content, relPath, consumer);
        }

        public async Task<T> Post<T>(
            HttpContent content,
            string relPath = "",
            Action<HttpResponseMessage> consumer = null
        )
        {
            return await WithClient<T>(
                nameof(Post),
                async (client, uri) => await client.PostAsync(uri, content),
                relPath,
                consumer
            );
        }

        public async Task Patch(HttpContent content, string relPath = "", Action<HttpResponseMessage> consumer = null)
        {
            await Patch<rvoid>(content, relPath, consumer);
        }

        public async Task<T> Patch<T>(
            HttpContent content,
            string relPath = "",
            Action<HttpResponseMessage> consumer = null
        )
        {
            return await WithClient<T>(
                nameof(Patch),
                async (client, uri) => await client.PatchAsync(uri, content),
                relPath,
                consumer
            );
        }

        public async Task Delete(string relPath = "", Action<HttpResponseMessage> consumer = null)
        {
            await Delete<rvoid>(relPath, consumer);
        }

        public async Task<T> Delete<T>(string relPath = "", Action<HttpResponseMessage> consumer = null)
        {
            return await WithClient<T>(
                nameof(Delete),
                async (client, uri) => await client.DeleteAsync(uri),
                relPath,
                consumer
            );
        }

        public void Dispose()
        {
            _client?.Dispose();
        }

        protected virtual async Task<T> WithClient<T>(
            string methodName,
            Func<HttpClient, Uri, Task<HttpResponseMessage>> action,
            string relPath = "",
            Action<HttpResponseMessage> consumer = null
        )
        {
            if (action == null)
            {
                throw new ArgumentNullException(nameof(action));
            }

            try
            {
                var combinedUri = new Uri(_client.BaseAddress, relPath);
                var response = await action(_client, combinedUri);
                consumer?.Invoke(response);

                var responseMessage = await response.Content.ReadAsStringAsync();
                if (response.IsSuccessStatusCode)
                {
                    _logger.LogDebug($"{methodName} done ({combinedUri})");
                    if (response.StatusCode == HttpStatusCode.NoContent)
                    {
                        return default;
                    }
                }
                else if (response.StatusCode == HttpStatusCode.TooManyRequests)
                {
                    throw new RateLimitedException(response, responseMessage);
                }
                else if (response.StatusCode == HttpStatusCode.Forbidden)
                {
                    throw new ForbiddenException();
                }
                else
                {
                    _logger.LogError($"Post failed: {responseMessage}");
                }

                if (typeof(T) == typeof(rvoid))
                {
                    return default;
                }

                try
                {
                    return JsonSerializer.Deserialize<T>(responseMessage);
                }
                catch (JsonException e)
                {
                    throw new ApiJsonException(e, responseMessage);
                }
            }
            catch (Exception e)
            {
                _logger.LogError($"{methodName} resulted in error: {e.Message}");
                return default;
            }
        }

        ~RestApi()
        {
            Dispose();
        }
    }
}
