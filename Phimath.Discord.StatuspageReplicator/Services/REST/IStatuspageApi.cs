using System;
using System.Threading.Tasks;

using Phimath.Discord.StatuspageReplicator.Models.Statuspage;

namespace Phimath.Discord.StatuspageReplicator.Services.REST
{
    public interface IStatuspageApi : IDisposable
    {
        protected const string ApiBase = "https://api.statuspage.io/v1/pages/";

        Task<Component[]> GetComponents();

        Task<Incident[]> GetIncidents(string subPath = null);

        public async Task<Incident[]> GetIncidentsUnresolved()
        {
            return await GetIncidents("unresolved");
        }

        public async Task<Incident[]> GetIncidentsScheduled()
        {
            return await GetIncidents("scheduled");
        }

        public async Task<Incident[]> GetIncidentsActive()
        {
            return await GetIncidents("active_maintenance");
        }

        public async Task<Incident[]> GetIncidentsUpcoming()
        {
            return await GetIncidents("upcoming");
        }
    }
}
