namespace Phimath.Discord.StatuspageReplicator.Services.REST
{
    public interface IGuildChannel
    {
        string GuildId { get; }

        string ChannelId { get; }

        string Name { get; }

        bool CanCrossPost { get; }
    }
}
