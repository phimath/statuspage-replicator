using System.Threading.Tasks;

using Microsoft.Extensions.Logging;

using Phimath.Discord.StatuspageReplicator.Extensions;
using Phimath.Discord.StatuspageReplicator.Models.Discord;
using Phimath.Discord.StatuspageReplicator.Services.REST.GenericImpl;

namespace Phimath.Discord.StatuspageReplicator.Services.REST.DiscordWebhookImpl
{
    public sealed class DiscordWebhook : IDiscordWebhook
    {
        private readonly RestApi _api;
        private readonly ILogger _logger;
        private readonly string _typeName;

        public DiscordWebhook(
            string targetUri,
            string targetName,
            string guild,
            string channel,
            string name,
            ILogger logger
        )
        {
            _logger = logger;
            ChannelId = channel;
            Name = name;
            GuildId = guild;
            _typeName = targetName;

            _api = new RestApi(targetUri, logger);
        }


        public async Task Execute(CreateWebhook data)
        {
            _logger.LogInformation($"Publishing {_typeName} to guild/channel: {GuildId}/{ChannelId}");
            await _api.Post(data.AsJsonContent());
        }

        public string ChannelId { get; }

        public string Name { get; }

        public bool CanCrossPost { get; } = false;

        public string GuildId { get; }

        public void Dispose()
        {
            _api?.Dispose();
        }

        ~DiscordWebhook()
        {
            Dispose();
        }
    }
}
