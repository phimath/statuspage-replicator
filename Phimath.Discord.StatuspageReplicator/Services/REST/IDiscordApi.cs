using System.Threading.Tasks;

using Phimath.Discord.StatuspageReplicator.Models.Discord;

namespace Phimath.Discord.StatuspageReplicator.Services.REST
{
    public interface IDiscordApi
    {
        protected const string Channel = "channels/{0}";
        protected const string ChannelMessages = "channels/{0}/messages";
        protected const string ChannelMessagesQuery = "channels/{0}/messages?limit=1";
        protected const string ChannelMessage = "channels/{0}/messages/{1}";
        protected const string ChannelMessageXPost = ChannelMessage + "/crosspost";

        Task<Message> CreateMessage(CreateMessage message, IGuildChannel channel);

        Task DeleteMessage(string channel, string message);

        Task<Message?> FindLastMessage(string channel, TargetType type);

        Task<Message> EditMessage(EditMessage message, string channel, string previous);

        Task<Channel> GetChannel(string channel);
    }
}
