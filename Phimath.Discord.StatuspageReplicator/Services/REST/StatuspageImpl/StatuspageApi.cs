using System.Net.Http.Headers;
using System.Threading.Tasks;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using Phimath.Discord.StatuspageReplicator.Models.Statuspage;

namespace Phimath.Discord.StatuspageReplicator.Services.REST.StatuspageImpl
{
    public class StatuspageApi : AuthorizedApi, IStatuspageApi
    {
        public StatuspageApi(IConfiguration configuration, ILogger<IStatuspageApi> logger)
            : base(configuration, logger, $"{IStatuspageApi.ApiBase}{configuration["Statuspage:PageID"]}/")
        {
        }

        public async Task<Component[]> GetComponents()
        {
            return await Get<Component[]>("components");
        }

        public async Task<Incident[]> GetIncidents(string subPath = null)
        {
            return await Get<Incident[]>(string.IsNullOrEmpty(subPath) ? "incidents" : $"incidents/{subPath}");
        }

        protected internal override AuthenticationHeaderValue GetAuthorization(IConfiguration configuration)
        {
            var token = configuration.GetValue<string>("Statuspage:ApiKey");
            return new AuthenticationHeaderValue("OAuth", token);
        }
    }
}
