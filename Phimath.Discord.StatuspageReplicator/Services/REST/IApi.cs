using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Phimath.Discord.StatuspageReplicator.Services.REST
{
    public interface IApi : IDisposable
    {
        public Task Post(HttpContent content, string relPath = "", Action<HttpResponseMessage> consumer = null);

        public Task<T> Post<T>(HttpContent content, string relPath = "", Action<HttpResponseMessage> consumer = null);

        public Task<T> Get<T>(string relPath = "", Action<HttpResponseMessage> consumer = null);

        public Task Patch(HttpContent content, string relPath = "", Action<HttpResponseMessage> consumer = null);

        public Task<T> Patch<T>(HttpContent content, string relPath = "", Action<HttpResponseMessage> consumer = null);

        public Task Delete(string relPath = "", Action<HttpResponseMessage> consumer = null);

        public Task<T> Delete<T>(string relPath = "", Action<HttpResponseMessage> consumer = null);
    }
}
