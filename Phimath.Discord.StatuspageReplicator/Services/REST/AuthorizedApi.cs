using System.Net.Http.Headers;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using Phimath.Discord.StatuspageReplicator.Services.REST.GenericImpl;

namespace Phimath.Discord.StatuspageReplicator.Services.REST
{
    public abstract class AuthorizedApi : RestApi
    {
        protected internal AuthorizedApi(IConfiguration configuration, ILogger logger, string baseUrl)
            : base(baseUrl, logger)
        {
            Initialize(configuration);
        }

        private void Initialize(IConfiguration configuration)
        {
            _client.DefaultRequestHeaders.Authorization = GetAuthorization(configuration);
        }

        protected internal abstract AuthenticationHeaderValue GetAuthorization(IConfiguration configuration);
    }
}
