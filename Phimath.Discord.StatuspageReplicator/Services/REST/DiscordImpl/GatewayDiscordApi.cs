using System;
using System.Threading.Tasks;

using Microsoft.Extensions.Logging;

using Phimath.Discord.StatuspageReplicator.Extensions;
using Phimath.Discord.StatuspageReplicator.Internal;
using Phimath.Discord.StatuspageReplicator.Models.Discord;
using Phimath.Discord.StatuspageReplicator.Services.Gateway;

namespace Phimath.Discord.StatuspageReplicator.Services.REST.DiscordImpl
{
    public sealed class GatewayDiscordApi : DiscordGateway, IDiscordApi
    {
        private const string RateLimitGlobal = "X-RateLimit-Global";
        private const string RateLimitLimit = "X-RateLimit-Limit";
        private const string RateLimitRemaining = "X-RateLimit-Remaining";
        private const string RateLimitReset = "X-RateLimit-Reset";
        private const string RateLimitResetAfter = "X-RateLimit-Reset-After";
        private const string RateLimitBucket = "X-RateLimit-Bucket";

        private readonly MetaService _meta;

        public GatewayDiscordApi(IServiceProvider sp, ILogger<IDiscordApi> logger, MetaService meta)
            : base(sp, logger)
        {
            _meta = meta;
        }

        public async Task<Message> CreateMessage(CreateMessage message, IGuildChannel channel)
        {
            var result = await Post<Message>(
                message.AsJsonContent(),
                string.Format(IDiscordApi.ChannelMessages, channel.ChannelId)
            );
            if (channel.CanCrossPost)
            {
                result = await Post<Message>(
                    new object().AsJsonContent(),
                    IDiscordApi.ChannelMessageXPost.Format(channel.ChannelId, result.Id)
                );
            }

            return result;
        }

        public async Task DeleteMessage(string channel, string message)
        {
            await Delete(string.Format(IDiscordApi.ChannelMessage, channel, message));
        }

        public async Task<Message?> FindLastMessage(string channel, TargetType type)
        {
            ulong? before = null;
            var compareTo = (string)typeof(MetaService).FieldForType(type)?.GetValue(_meta);

            var waitFailed = 0;

            while (true)
            {
                var queryPath = string.Format(IDiscordApi.ChannelMessagesQuery, channel)
                                + (before.HasValue ? $"&before={before.Value}" : string.Empty);

                var r = await Get<Message[]>(queryPath);
                if (r.Length == 0)
                {
                    return null;
                }

                var m = r[0];

                if (before.HasValue && m.Id.ToULong() == before.Value)
                {
                    return null;
                }

                before = m.Id.ToULong();

                if (m.Embeds.Length == 0)
                {
                    goto waitNext;
                }

                var e = m.Embeds[0];
                if (e.Title == compareTo)
                {
                    return m;
                }

                waitNext:
                waitFailed = Math.Min(waitFailed + 1, 6);
                _logger.LogInformation(
                    $"Waiting after {waitFailed} failed attempt(s) to find the last message for target {type}"
                );
                await Task.Delay(TimeSpan.FromSeconds(waitFailed * 5));
            }
        }

        public async Task<Message> EditMessage(EditMessage message, string channel, string previous)
        {
            return await Patch<Message>(
                message.AsJsonContent(),
                string.Format(IDiscordApi.ChannelMessage, channel, previous)
            );
        }

        public async Task<Channel> GetChannel(string channel)
        {
            return await Get<Channel>(string.Format(IDiscordApi.Channel, channel));
        }
    }
}
