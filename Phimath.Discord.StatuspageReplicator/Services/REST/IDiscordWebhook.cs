using System;
using System.Threading.Tasks;

using Phimath.Discord.StatuspageReplicator.Models.Discord;

namespace Phimath.Discord.StatuspageReplicator.Services.REST
{
    public interface IDiscordWebhook : IGuildChannel, IDisposable
    {
        Task Execute(CreateWebhook data);
    }
}
