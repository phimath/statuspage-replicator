using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using Phimath.Discord.StatuspageReplicator.Extensions;
using Phimath.Discord.StatuspageReplicator.Models.Statuspage;

namespace Phimath.Discord.StatuspageReplicator.Services
{
    public class Cache : IDisposable
    {
        private const string CacheFileName = "cache.json";

        private readonly string _location;
        private readonly ILogger _logger;

        /// <summary>
        ///     Maps the component id to the updated at
        /// </summary>
        private Dictionary<string, DateTime> _componentCache;

        private bool _dirty;

        /// <summary>
        ///     Maps the incident id to the updated at
        /// </summary>
        private Dictionary<string, DateTime> _incidentCache;

        /// <summary>
        ///     Maps the incident id to the updated at
        /// </summary>
        private Dictionary<ulong, GuildMessageCache> _messageCache;

        public Cache(IConfiguration configuration, ILogger<Cache> logger)
        {
            _logger = logger;

            _location = Path.Join(configuration.GetValue<string>("App:DataDirectory"), CacheFileName);
            if (_location == null)
            {
                _location = Path.Join(Environment.CurrentDirectory, CacheFileName);
                _logger.LogInformation("No cache location defined. Defaulting to current working directory");
            }

            _logger.LogInformation($"Using cache location: {_location}");

            Directory.CreateDirectory(Path.GetDirectoryName(_location));
            TryLoad();
        }

        public void Dispose()
        {
        }

        private bool TryLoad()
        {
            if (File.Exists(_location))
            {
                try
                {
                    Read(File.ReadAllText(_location), this);
                    return true;
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "Error loading cache: ");
                }
            }

            _componentCache = new Dictionary<string, DateTime>();
            _incidentCache = new Dictionary<string, DateTime>();
            _messageCache = new Dictionary<ulong, GuildMessageCache>();
            _dirty = true;
            Dump();
            return false;
        }

        private static void Read(string json, Cache target)
        {
            target._componentCache ??= new Dictionary<string, DateTime>();
            target._incidentCache ??= new Dictionary<string, DateTime>();
            target._messageCache ??= new Dictionary<ulong, GuildMessageCache>();

            try
            {
                using var doc = JsonDocument.Parse(json);

                foreach (var kvp in doc.RootElement.GetProperty("components").EnumerateObject())
                {
                    target._componentCache.Add(kvp.Name, kvp.Value.GetDateTime());
                }

                foreach (var kvp in doc.RootElement.GetProperty("incidents").EnumerateObject())
                {
                    target._incidentCache.Add(kvp.Name, kvp.Value.GetDateTime());
                }

                foreach (var kvp in doc.RootElement.GetProperty("messages").EnumerateObject())
                {
                    target._messageCache.Add(
                        kvp.Name.ToULong(),
                        new GuildMessageCache(
                            kvp.Value.EnumerateArray()
                               .Select(
                                   value =>
                                   {
                                       // ReSharper disable once SwitchExpressionHandlesSomeKnownEnumValuesWithExceptionInDefault
                                       return value.ValueKind switch
                                       {
                                           // ReSharper disable once RedundantCast
                                           JsonValueKind.Null => (ulong?)null,
                                           JsonValueKind.Number => value.GetUInt64(),
                                           _ => throw new ArgumentOutOfRangeException(),
                                       };
                                   }
                               )
                               .ToArray(),
                            target.SetDirty
                        )
                    );
                }
            }
            catch (Exception)
            {
                target._logger.LogWarning("Cannot read cache, initializing new cache");
            }
        }

        internal async void Dump()
        {
            if (!_dirty)
            {
                return;
            }

            var options = new JsonSerializerOptions();
            options.Converters.Add(new CacheConverter());
            options.WriteIndented = true;
            var content = JsonSerializer.Serialize(this, options);
            await File.WriteAllTextAsync(_location, content);
        }

        internal bool IsComponentUpdated(Component c)
        {
            var isCached = _componentCache.TryGetValue(c.Id, out var lastUpdate);
            if (isCached)
            {
                // If no update is present do not post
                if (c.UpdatedAt == lastUpdate)
                {
                    return false;
                }

                // If update is present, update it in the cache
                _componentCache[c.Id] = c.UpdatedAt;
                _dirty = true;
                return true;
            }

            // Add to cache
            _componentCache.Add(c.Id, c.UpdatedAt);
            _dirty = true;
            return true;
        }

        internal bool IsIncidentUpdated(Incident i)
        {
            var isCached = _incidentCache.TryGetValue(i.Id, out var lastUpdate);
            if (isCached)
            {
                // If no update is present do not post
                if (i.UpdatedAt == lastUpdate)
                {
                    return false;
                }

                // If update is present, update it in the cache
                _incidentCache[i.Id] = i.UpdatedAt;
                _dirty = true;
                return true;
            }

            // Add to cache
            _incidentCache.Add(i.Id, i.UpdatedAt);
            _dirty = true;
            return true;
        }

        public GuildMessageCache GetGuildCache(string guild)
        {
            var key = guild.ToULong();
            if (_messageCache.ContainsKey(key))
            {
                return _messageCache[key];
            }

            var cache = GuildMessageCache.Initialize(SetDirty);
            _messageCache[key] = cache;
            return cache;
        }

        private void SetDirty()
        {
            _dirty = true;
        }

        public sealed class GuildMessageCache
        {
            private readonly Action _onDirty;

            internal readonly ulong?[] _values;

            internal GuildMessageCache(ulong?[] values, Action onDirty)
            {
                _values = values;
                _onDirty = onDirty;
            }

            internal ulong? this[TargetType t]
            {
                get => GetItem(t);
                set
                {
                    ref var target = ref GetItem(t);
                    if (target.HasValue && !value.HasValue
                        || !target.HasValue && value.HasValue
                        || target.HasValue && value.HasValue && target.Value != value.Value)
                    {
                        _onDirty();
                    }

                    target = value;
                }
            }

            internal static GuildMessageCache Initialize(Action dirtySetter)
            {
                return new(new ulong?[5], dirtySetter);
            }

            private ref ulong? GetItem(TargetType t)
            {
                switch (t)
                {
                    case TargetType.Components:
                        return ref _values[0];
                    case TargetType.Incidents:
                        return ref _values[1];
                    case TargetType.Maintenance:
                        return ref _values[2];
                    case TargetType.Shards:
                        return ref _values[3];
                    case TargetType.Scheduled:
                        return ref _values[4];
                    default:
                        throw new ArgumentOutOfRangeException(nameof(t), t, null);
                }
            }
        }

        private class CacheConverter : JsonConverter<Cache>
        {
            public override Cache Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
            {
                throw new NotSupportedException();
            }

            public override void Write(Utf8JsonWriter writer, Cache value, JsonSerializerOptions options)
            {
                writer.WriteStartObject();
                writer.WriteStartObject("components");

                foreach (var (k, v) in value._componentCache)
                {
                    writer.WritePropertyName(k);
                    writer.WriteStringValue(v);
                }

                writer.WriteEndObject();

                writer.WriteStartObject("incidents");

                foreach (var (k, v) in value._incidentCache)
                {
                    writer.WritePropertyName(k);
                    writer.WriteStringValue(v);
                }

                writer.WriteEndObject();

                writer.WriteStartObject("messages");

                foreach (var (k, v) in value._messageCache)
                {
                    writer.WriteStartArray(k.ToString());
                    foreach (var vValue in v._values)
                    {
                        if (vValue.HasValue)
                        {
                            writer.WriteNumberValue(vValue.Value);
                        }
                        else
                        {
                            writer.WriteNullValue();
                        }
                    }

                    writer.WriteEndArray();
                }

                writer.WriteEndObject();
                writer.WriteEndObject();
            }
        }
    }
}
