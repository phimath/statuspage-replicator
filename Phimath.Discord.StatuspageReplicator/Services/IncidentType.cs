// Copyright 2021 (c) phimath.
// All rights reserved if not stated otherwise or licensed under one or more agreements.
// If applicable, license agreements can be found in the top most level of the source repository.

namespace Phimath.Discord.StatuspageReplicator.Services
{
    internal enum IncidentType : byte
    {
        /// <summary>
        ///     Current, unresolved non-maintenance incident
        /// </summary>
        UnresolvedIncident,

        /// <summary>
        ///     Active maintenance incident
        /// </summary>
        ActiveMaintenance,

        /// <summary>
        ///     Upcoming incident
        /// </summary>
        UpcomingIncident,

        /// <summary>
        ///     Scheduled incident
        /// </summary>
        ScheduledIncident,
    }
}
