using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Extensions.Configuration;

using Phimath.Discord.StatuspageReplicator.Internal;
using Phimath.Discord.StatuspageReplicator.Models.Discord;
using Phimath.Discord.StatuspageReplicator.Models.Statuspage;

namespace Phimath.Discord.StatuspageReplicator.Services
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public sealed class MetaService
    {
        private static readonly Func<Emoji, string> DefaultEmojis = emoji => emoji switch
        {
            Emoji.Empty => "574958002067472384",
            Emoji.Green => "571989790908612618",
            Emoji.Blue => "574958002235375637",
            Emoji.Yellow => "571989764861853706",
            Emoji.Red => "571989744460759070",
            Emoji.Orange => "571989764861853706",
            _ => throw new ArgumentOutOfRangeException(nameof(emoji), emoji, null),
        };

        [ForType(TargetType.Maintenance)]
        private readonly string _activeHeader;

        private readonly Dictionary<string, Func<Emoji, string>> _customEmojis;
        private readonly string _footerExtra;

        [ForType(TargetType.Incidents)]
        private readonly string _incidentHeader;

        private readonly bool _postEnabled;

        [ForType(TargetType.Scheduled)]
        private readonly string _scheduledHeader;

        [ForType(TargetType.Shards)]
        private readonly string _shardStatusHeader;

        [ForType(TargetType.Components)]
        private readonly string _statusHeader;

        public MetaService(IConfiguration configuration)
        {
            _postEnabled = configuration.GetValue<bool?>("Http:EnablePost").GetValueOrDefault(true);

            _footerExtra = configuration["Embeds:Footer"] ?? string.Empty;
            _statusHeader = configuration["Embeds:StatusHeader"] ?? "System Status";
            _shardStatusHeader = configuration["Embeds:ShardStatusHeader"] ?? "Shard Status";
            _scheduledHeader = configuration["Embeds:ScheduledHeader"] ?? "Scheduled Incidents";
            _activeHeader = configuration["Embeds:ActiveHeader"] ?? "Active Maintenance";
            _incidentHeader = configuration["Embeds:IncidentHeader"] ?? "Active Incidents";

            var children = configuration.GetSection("Icons").GetChildren().ToArray();
            _customEmojis = new Dictionary<string, Func<Emoji, string>>(children.Length + 1);
            foreach (var serverConfig in children)
            {
                /*_customEmojis.Add(serverConfig.Key, emoji =>
                {
                    return emoji switch
                    {
                        Emoji.EMPTY => serverConfig.GetValue<string>(nameof(Emoji.EMPTY)),
                        Emoji.GREEN => serverConfig.GetValue<string>(nameof(Emoji.GREEN)),
                        Emoji.BLUE => serverConfig.GetValue<string>(nameof(Emoji.BLUE)),
                        Emoji.YELLOW => serverConfig.GetValue<string>(nameof(Emoji.YELLOW)),
                        Emoji.RED => serverConfig.GetValue<string>(nameof(Emoji.RED)),
                        Emoji.ORANGE => serverConfig.GetValue<string>(nameof(Emoji.ORANGE)),
                        _ => throw new ArgumentOutOfRangeException(nameof(emoji), emoji, null)
                    };
                });*/
            }

            // Allow default to be specified from the config file
            if (!_customEmojis.ContainsKey(string.Empty))
            {
                _customEmojis.Add(string.Empty, DefaultEmojis);
            }
        }

        private string EmojiToMention(Emoji emoji, string guildId = "")
        {
            string valueId;

            if (_customEmojis.TryGetValue(guildId, out var emojiSelector))
            {
                valueId = emojiSelector(emoji);
            }
            else
            {
                valueId = DefaultEmojis(emoji);
            }

            return $"<:{emoji.ToString().ToLowerInvariant()}:{valueId}>";
        }

        private Emoji StatusToEmoji(string status)
        {
            return status switch
            {
                "operational" => Emoji.Green,
                "degraded_performance" => Emoji.Yellow,
                "partial_outage" => Emoji.Orange,
                "major_outage" => Emoji.Red,
                "under_maintenance" => Emoji.Blue,
                _ => throw new ArgumentOutOfRangeException(nameof(status), status, null),
            };
        }

        private string StatusSanitizer(string status)
        {
            return string.Join(
                ' ',
                status.Replace('_', ' ').Split(' ').Select(s => s.Substring(0, 1).ToUpperInvariant() + s.Substring(1))
            );
        }

        private int ImpactToColor(Impact impact)
        {
            return impact switch
            {
                Impact.None => 0x00FF00,
                Impact.Minor => 0xF1C40F,
                Impact.Major => 0xFFA500,
                Impact.Critical => 0xFF0000,
                Impact.Maintenance => 0x3498DB,
                _ => throw new ArgumentOutOfRangeException(nameof(impact), impact, null),
            };
        }

        internal Embed BuildStatus(
            Impact maxImpact,
            (Component component, List<Incident> openIncidents, List<Incident> underMaintenance)[] components,
            string guild = ""
        )
        {
            return new()
            {
                Title = _statusHeader,
                Type = "rich",
                Color = ImpactToColor(maxImpact),
                Footer = new EmbedFooter { Name = $"{DateTime.UtcNow:yyyy-MM-dd HH:mm:ss} UTC — {_footerExtra}" },
                Fields = components.Select(
                                       c => new EmbedField
                                       {
                                           Inline = false,
                                           Name =
                                               $"{EmojiToMention(StatusToEmoji(c.component.Status), guild)} {c.component.Name} — {StatusSanitizer(c.component.Status)}",
                                           Value = c.openIncidents.Any()
                                               ? string.Join(
                                                   "\n",
                                                   c.openIncidents.Select(
                                                       i =>
                                                           $"{EmojiToMention(Emoji.Empty, guild)} {(i.ImpactInternal != Impact.Maintenance ? "INCD" : "MNTC")}: {i.Name} ({i.Link})"
                                                   )
                                               )
                                               : EmojiToMention(Emoji.Empty, guild),
                                       }
                                   )
                                   .ToArray(),
                _targetType = TargetType.Components,
            };
        }

        internal Embed BuildScheduledMaintenance(Incident[] scheduledMaintenance)
        {
            return new()
            {
                Title = _scheduledHeader,
                Type = "rich",
                Color = scheduledMaintenance.Any() ? ImpactToColor(Impact.Maintenance) : 0xD7D7D7,
                Footer = new EmbedFooter { Name = $"{DateTime.UtcNow:yyyy-MM-dd HH:mm:ss} UTC — {_footerExtra}" },
                Description = $"{scheduledMaintenance.Length} maintenance event(s) scheduled",
                Fields = scheduledMaintenance.Any()
                    ? scheduledMaintenance.Select(
                                              c => new EmbedField
                                              {
                                                  Inline = false,
                                                  Name = $"{c.Name} ({c.Link})",
                                                  Value =
                                                      $"Start: {c.ScheduledFor:yyyy-MM-dd HH:mm:ss} UTC\nDuration: {c.ScheduledUntil - c.ScheduledFor}",
                                              }
                                          )
                                          .ToArray()
                    : Array.Empty<EmbedField>(),
                _targetType = TargetType.Scheduled,
            };
        }

        internal Embed BuildActiveMaintenance(Incident[] activeMaintenance)
        {
            return new()
            {
                Title = _activeHeader,
                Type = "rich",
                Color = activeMaintenance.Any() ? ImpactToColor(Impact.Maintenance) : 0xD7D7D7,
                Footer = new EmbedFooter { Name = $"{DateTime.UtcNow:yyyy-MM-dd HH:mm:ss} UTC — {_footerExtra}" },
                Description = $"{activeMaintenance.Length} active maintenance event(s)",
                Fields = activeMaintenance.Any()
                    ? activeMaintenance.Select(
                                           c => new EmbedField
                                           {
                                               Inline = false,
                                               Name = $"{c.Name} ({c.Link})",
                                               Value =
                                                   $"Start: {c.ScheduledFor ?? c.CreatedAt:yyyy-MM-dd HH:mm:ss} UTC\nPlanned until: {c.ScheduledUntil:yyyy-MM-dd HH:mm:ss}\nCurrent Duration: {DateTime.UtcNow - (c.ScheduledFor ?? c.CreatedAt)}",
                                           }
                                       )
                                       .ToArray()
                    : Array.Empty<EmbedField>(),
                _targetType = TargetType.Maintenance,
            };
        }

        internal Embed BuildIncident(Incident[] incidents, Impact maxImpact)
        {
            return new()
            {
                Title = _incidentHeader,
                Type = "rich",
                Color = incidents.Any() ? ImpactToColor(maxImpact) : 0xD7D7D7,
                Footer = new EmbedFooter { Name = $"{DateTime.UtcNow:yyyy-MM-dd HH:mm:ss} UTC — {_footerExtra}" },
                Description = $"{incidents.Length} active incident(s)",
                Fields = incidents.Any()
                    ? incidents.Select(
                                   c => new EmbedField
                                   {
                                       Inline = false,
                                       Name = $"{c.Name} ({c.Link})",
                                       Value = string.Join(
                                           '\n',
                                           c.IncidentUpdates.Select(
                                               u =>
                                                   $"⇒ **{StatusSanitizer(u.Status)}** ({u.DisplayAt:yyyy-MM-dd HH:mm:ss} - {u.Body}"
                                           )
                                       ),
                                   }
                               )
                               .ToArray()
                    : Array.Empty<EmbedField>(),
                _targetType = TargetType.Incidents,
            };
        }

        internal Embed BuildShardStatus(
            Impact maxImpact,
            (Component component, List<Incident> openIncidents, List<Incident> underMaintenance)[] shards,
            string guild = ""
        )
        {
            return new()
            {
                Title = _shardStatusHeader,
                Type = "rich",
                Color = ImpactToColor(maxImpact),
                Footer = new EmbedFooter { Name = $"{DateTime.UtcNow:yyyy-MM-dd HH:mm:ss} UTC — {_footerExtra}" },
                Fields = shards.Select(
                                   c => new EmbedField
                                   {
                                       Inline = false,
                                       Name =
                                           $"{EmojiToMention(StatusToEmoji(c.component.Status), guild)} {c.component.Name} — {StatusSanitizer(c.component.Status)}",
                                       Value = c.openIncidents.Any()
                                           ? string.Join(
                                               "\n",
                                               c.openIncidents.Select(
                                                   i =>
                                                       $"{EmojiToMention(Emoji.Empty, guild)} {(i.ImpactInternal != Impact.Maintenance ? "INCD" : "MNTC")}: {i.Name} ({i.Link})"
                                               )
                                           )
                                           : EmojiToMention(Emoji.Empty, guild),
                                   }
                               )
                               .ToArray(),
                _targetType = TargetType.Shards,
            };
        }
    }
}
