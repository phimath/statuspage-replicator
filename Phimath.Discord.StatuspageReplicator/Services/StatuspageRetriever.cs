using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

using Phimath.Discord.StatuspageReplicator.Models.Statuspage;
using Phimath.Discord.StatuspageReplicator.Services.Messaging;
using Phimath.Discord.StatuspageReplicator.Services.REST;

namespace Phimath.Discord.StatuspageReplicator.Services
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class StatuspageRetriever : BackgroundService
    {
        private readonly IStatuspageApi _api;
        private readonly Cache _cache;
        private readonly Communicator _communicator;
        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;
        private readonly bool _publishAlways;

        public StatuspageRetriever(
            Communicator communicator,
            Cache cache,
            IConfiguration configuration,
            ILoggerFactory lf,
            IStatuspageApi api
        )
        {
            _api = api;
            _communicator = communicator;
            _cache = cache;
            _configuration = configuration;
            _logger = lf.CreateLogger("Replicator.Polling");
            _publishAlways = configuration.GetValue<bool?>("App:Targets:PublishAlways").GetValueOrDefault(false);
            _logger.LogInformation($"Using PublishAlways={_publishAlways}");
        }

        private int DelayMilliseconds => _configuration.GetValue<int>("App:StatuspagePollingSeconds") * 1000;

        private static void SetIncidentType(ref Incident[] incidents, IncidentType type)
        {
            if (incidents == null || !incidents.Any())
            {
                return;
            }

            incidents = incidents.Select(
                                     i =>
                                     {
                                         i.Type = type;
                                         return i;
                                     }
                                 )
                                 .ToArray();
        }

        protected override async Task ExecuteAsync(CancellationToken cancel)
        {
            while (!cancel.IsCancellationRequested)
            {
                try
                {
                    _logger.LogTrace("Fetching Components at: {time}", DateTimeOffset.Now);

                    var sendComponentUpdate = _publishAlways;
                    var sendShardUpdate = _publishAlways;
                    var sendIncidentUpdate = _publishAlways;
                    var sendMaintenanceUpdate = _publishAlways;

                    var allComponents = await _api.GetComponents();

                    var components = allComponents.Where(c => c.GroupId == null).ToArray();
                    var shards = allComponents.Where(c => c.GroupId != null).ToArray(); // TODO: Specific id here

                    var componentsToIncident =
                        new Dictionary<string, List<Incident>>(components.Length + shards.Length); // TODO: Improve this
                    var componentsToMaintenance =
                        new Dictionary<string, List<Incident>>(components.Length + shards.Length); // TODO: Improve this

                    foreach (var component in components)
                    {
                        sendComponentUpdate = _cache.IsComponentUpdated(component) || sendComponentUpdate;
                        componentsToIncident.Add(component.Id, new List<Incident>());
                        componentsToMaintenance.Add(component.Id, new List<Incident>());
                    }

                    foreach (var component in shards)
                    {
                        sendShardUpdate = _cache.IsComponentUpdated(component) || sendShardUpdate;
                        componentsToIncident.Add(component.Id, new List<Incident>());
                        componentsToMaintenance.Add(component.Id, new List<Incident>());
                    }

                    byte computeImpact = 0;

                    Incident[] ProcessIncidents(
                        Incident[] innerIncidents,
                        IncidentType type,
                        Dictionary<string, List<Incident>> dictionary
                    )
                    {
                        _logger.LogTrace($"Fetching {type} at: {DateTime.Now}");

                        if (innerIncidents == null)
                        {
                            return new Incident[0];
                        }

                        SetIncidentType(ref innerIncidents, type);

                        if (innerIncidents.Any())
                        {
                            computeImpact = Math.Max(
                                computeImpact,
                                innerIncidents.Max(i => (byte)Enum.Parse<Impact>(i.ImpactOverride ?? i.Impact, true))
                            );

                            foreach (var incident in innerIncidents)
                            {
                                var update = _cache.IsIncidentUpdated(incident);

                                foreach (var component in incident.Components)
                                {
                                    if (update)
                                    {
                                        if (component.GroupId == null)
                                        {
                                            sendComponentUpdate = true;
                                        }
                                        else
                                        {
                                            sendShardUpdate = true;
                                        }
                                    }

                                    dictionary[component.Id].Add(incident);
                                }

                                if (type == IncidentType.ScheduledIncident)
                                {
                                    sendMaintenanceUpdate = update || sendMaintenanceUpdate;
                                }
                                else
                                {
                                    sendIncidentUpdate = update || sendIncidentUpdate;
                                }
                            }
                        }

                        return innerIncidents;
                    }

                    // Get the unresolved incidents for calculating the current color
                    var unresolved = ProcessIncidents(
                        await _api.GetIncidentsUnresolved(),
                        IncidentType.UnresolvedIncident,
                        componentsToIncident
                    );
                    var scheduledMaintenance = ProcessIncidents(
                        await _api.GetIncidentsScheduled(),
                        IncidentType.ScheduledIncident,
                        componentsToMaintenance
                    );
                    var activeMaintenance =
                        ProcessIncidents(
                                await _api.GetIncidentsActive(),
                                IncidentType.ActiveMaintenance,
                                componentsToIncident
                            )
                            .Concat(unresolved.Where(i => i.ImpactInternal == Impact.Maintenance))
                            .ToArray();
                    var upcoming = ProcessIncidents(
                        await _api.GetIncidentsUpcoming(),
                        IncidentType.UpcomingIncident,
                        componentsToIncident
                    );

                    unresolved = unresolved.Where(i => i.ImpactInternal != Impact.Maintenance).ToArray();

                    var incidents = unresolved.Concat(upcoming).ToArray();

                    var maxImpact = (Impact)computeImpact;

                    _cache.Dump();

                    if (sendComponentUpdate)
                    {
                        _logger.LogInformation("Sending status event");
                        _communicator.OnStatus(
                            maxImpact,
                            components,
                            componentsToIncident,
                            componentsToMaintenance,
                            scheduledMaintenance
                        );
                    }

                    if (sendShardUpdate)
                    {
                        _logger.LogInformation("Sending shard event");
                        _communicator.OnShardStatus(
                            shards,
                            componentsToIncident,
                            componentsToMaintenance,
                            scheduledMaintenance
                        );
                    }

                    if (sendIncidentUpdate)
                    {
                        _logger.LogInformation("Sending incident event");
                        _communicator.OnIncidents(incidents, maxImpact);
                    }

                    if (sendMaintenanceUpdate)
                    {
                        _logger.LogInformation("Sending maintenance event");
                        _communicator.OnMaintenance(activeMaintenance, scheduledMaintenance);
                    }

                    await Task.Delay(DelayMilliseconds, cancel);
                }
                catch (TaskCanceledException)
                {
                    Dispose();
                }
                catch (Exception e)
                {
                    _logger.LogCritical(e, "Error in Status retrieving");
                }
            }
        }

        public override void Dispose()
        {
            _api.Dispose();
            base.Dispose();
        }
    }
}
