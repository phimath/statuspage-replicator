using System;

namespace Phimath.Discord.StatuspageReplicator.Extensions
{
    internal static class ParseExtensions
    {
        internal static int ToInt(this string s)
        {
            return int.Parse(s);
        }

        internal static ulong ToULong(this string s)
        {
            return ulong.Parse(s);
        }

        internal static long ToLong(this string s)
        {
            return long.Parse(s);
        }

        internal static long? ToLongSafe(this string s)
        {
            return s == null ? (long?)null : long.Parse(s);
        }

        internal static DateTime AsEpoch(this long epoch)
        {
            return DateTime.UnixEpoch.AddSeconds(epoch);
        }
    }
}
