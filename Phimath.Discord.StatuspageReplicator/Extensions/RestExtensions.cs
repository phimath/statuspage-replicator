// Copyright 2021 (c) phimath.
// All rights reserved if not stated otherwise or licensed under one or more agreements.
// If applicable, license agreements can be found in the top most level of the source repository.

using System.Net.Http;
using System.Text;
using System.Text.Json;

namespace Phimath.Discord.StatuspageReplicator.Extensions
{
    public static class RestExtensions
    {
        public static HttpContent AsJsonContent(this object input)
        {
            return new StringContent(
                JsonSerializer.Serialize(input, Program.NullableJsonOpts),
                Encoding.UTF8,
                "application/json"
            );
        }
    }
}
