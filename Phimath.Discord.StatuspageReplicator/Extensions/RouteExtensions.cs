// Copyright 2021 (c) phimath.
// All rights reserved if not stated otherwise or licensed under one or more agreements.
// If applicable, license agreements can be found in the top most level of the source repository.

namespace Phimath.Discord.StatuspageReplicator.Extensions
{
    public static class RouteExtensions
    {
        public static string Format(this string template, params object[] fillIns)
        {
            return string.Format(template, fillIns);
        }
    }
}
