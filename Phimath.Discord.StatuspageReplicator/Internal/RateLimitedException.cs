// Copyright 2021 (c) phimath.
// All rights reserved if not stated otherwise or licensed under one or more agreements.
// If applicable, license agreements can be found in the top most level of the source repository.

using System;
using System.Net.Http;

namespace Phimath.Discord.StatuspageReplicator.Internal
{
    public class RateLimitedException : ApplicationException
    {
        internal readonly string _content;
        internal readonly HttpResponseMessage _response;

        public RateLimitedException(HttpResponseMessage response, string content = null)
        {
            _response = response;
            _content = content;
            _content = content ?? response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
        }
    }
}
