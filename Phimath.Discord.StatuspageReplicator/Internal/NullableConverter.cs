using System;
using System.Runtime.CompilerServices;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Phimath.Discord.StatuspageReplicator.Internal
{
    public class NullableConverter<T> : JsonConverter<T?> where T : struct
    {
        private readonly NullableConverter _impl = new();

        public override T? Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            return (T?)_impl.Read(ref reader, typeToConvert, options);
        }

        public override void Write(Utf8JsonWriter writer, T? value, JsonSerializerOptions options)
        {
            _impl.Write(writer, value, options);
        }
    }

    public class NullableConverter : JsonConverter<object>
    {
        private static readonly JavaScriptEncoder NoneEncoderInstance = new NoneEncoder();

        public override bool CanConvert(Type typeToConvert)
        {
            return typeToConvert.IsConstructedGenericType
                   && typeToConvert.GetGenericTypeDefinition() == typeof(Nullable<>);
        }

        public override object Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            return null;
        }

        public override void Write(Utf8JsonWriter writer, object value, JsonSerializerOptions options)
        {
            switch (value)
            {
                case null:
                    writer.WriteNullValue();
                    break;
                case string v:
                    writer.WriteStringValue(v);
                    break;
                case DateTime v:
                    writer.WriteStringValue(v);
                    break;
                case DateTimeOffset v:
                    writer.WriteStringValue(v);
                    break;
                case Guid v:
                    writer.WriteStringValue(v);
                    break;
                case decimal v:
                    writer.WriteNumberValue(v);
                    break;
                case double v:
                    writer.WriteNumberValue(v);
                    break;
                case float v:
                    writer.WriteNumberValue(v);
                    break;
                case int v:
                    writer.WriteNumberValue(v);
                    break;
                case long v:
                    writer.WriteNumberValue(v);
                    break;
                case uint v:
                    writer.WriteNumberValue(v);
                    break;
                case ulong v:
                    writer.WriteNumberValue(v);
                    break;
                case ushort v:
                    writer.WriteNumberValue(v);
                    break;
                case byte v:
                    writer.WriteNumberValue(v);
                    break;
                case bool v:
                    writer.WriteBooleanValue(v);
                    break;
                default:
                {
                    var type = value.GetType();

                    if (type.IsArray)
                    {
                        writer.WriteStartArray();
                        writer.WriteEndArray();
                    }
                    else if (type.IsConstructedGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                    {
                        var realType = type.GenericTypeArguments[0];
                        dynamic innerValue = type.GetProperty("Value")?.GetValue(value);
                        using var tmp = JsonDocument.Parse(JsonSerializer.Serialize(innerValue));
                        tmp.WriteTo(writer);
                    }
                    else if (type.IsValueType)
                    {
                        using var tmp = JsonDocument.Parse(JsonSerializer.Serialize(value));
                        tmp.WriteTo(writer);
                    }
                    else
                    {
                        throw new ArgumentOutOfRangeException(nameof(value), $"Unsupported type: {type.FullName}");
                    }

                    break;
                }
            }
        }

        private sealed class NoneEncoder : JavaScriptEncoder
        {
            public override int MaxOutputCharactersPerInputCharacter => 1;

            public override unsafe bool TryEncodeUnicodeScalar(
                int unicodeScalar,
                char* buffer,
                int bufferLength,
                out int numberOfCharactersWritten
            )
            {
                if (bufferLength < 1)
                {
                    numberOfCharactersWritten = 0;
                    return false;
                }

                *buffer = (char)unicodeScalar;
                numberOfCharactersWritten = 1;
                return true;
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public override bool WillEncode(int unicodeScalar)
            {
                return false;
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            public override unsafe int FindFirstCharacterToEncode(char* text, int textLength)
            {
                if (text == null)
                {
                    throw new ArgumentNullException(nameof(text));
                }

                return 0;
            }
        }
    }
}
