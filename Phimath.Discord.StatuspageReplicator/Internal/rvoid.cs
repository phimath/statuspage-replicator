// ReSharper disable All

using System;

namespace Phimath.Statuspage.Replicator.Internal
{
    internal sealed class rvoid
    {
        private rvoid()
        {
            throw new NotSupportedException("Do not instantiate void type");
        }
    }
}
