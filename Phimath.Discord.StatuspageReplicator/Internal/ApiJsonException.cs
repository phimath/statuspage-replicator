using System;
using System.Text.Json;

namespace Phimath.Discord.StatuspageReplicator.Internal
{
    public class ApiJsonException : ApplicationException
    {
        public ApiJsonException(JsonException e, string json)
            : base(
                $"Error on a JSON operation: {e.Message}"
                + $"{Environment.NewLine}>>"
                + $"{Environment.NewLine}{json}"
                + $"{Environment.NewLine}<<",
                e
            )
        {
            Json = json;
        }

        public string Json { get; }
    }
}
