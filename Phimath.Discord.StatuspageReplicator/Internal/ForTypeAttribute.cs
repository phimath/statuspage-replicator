using System;
using System.Linq;
using System.Reflection;

using Phimath.Discord.StatuspageReplicator.Services;

namespace Phimath.Discord.StatuspageReplicator.Internal
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Class)]
    internal sealed class ForTypeAttribute : Attribute
    {
        internal readonly TargetType _type;

        internal ForTypeAttribute(TargetType type)
        {
            _type = type;
        }
    }

    internal static class TypeExtensions
    {
        internal static FieldInfo[] FieldsForType(this Type target, TargetType type)
        {
            return target
                   .GetFields(
                       BindingFlags.GetField | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance
                   )
                   .Where(
                       f => f.CustomAttributes.Any()
                            && f.GetCustomAttributes<ForTypeAttribute>().Any(a => a._type == type)
                   )
                   .ToArray();
        }

        internal static FieldInfo FieldForType(this Type target, TargetType type)
        {
            var found = target.FieldsForType(type);
            if (!found.Any())
            {
                return null;
            }

            if (found.Length > 1)
            {
                throw new ApplicationException(
                    $"Wrongly defined \"ForTypeAttribute\": Matches more than one field ({string.Join(", ", found.Select(f => f.Name))})"
                );
            }

            return found[0];
        }

        internal static TargetType? GetTargetType(this Type target)
        {
            var found = target.GetCustomAttributes<ForTypeAttribute>().ToArray();
            if (!found.Any())
            {
                return null;
            }

            if (found.Length > 1)
            {
                throw new ApplicationException("Wrongly defined \"ForTypeAttribute\": Matches more than one annotated");
            }

            return found[0]._type;
        }
    }
}
