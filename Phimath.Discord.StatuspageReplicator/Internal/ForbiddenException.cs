using System;

namespace Phimath.Discord.StatuspageReplicator.Internal
{
    public sealed class ForbiddenException : ApplicationException
    {
    }
}
