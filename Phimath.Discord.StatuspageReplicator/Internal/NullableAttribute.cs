using System;

namespace Phimath.Discord.StatuspageReplicator.Internal
{
    [AttributeUsage(AttributeTargets.Struct)]
    public sealed class NullableAttribute : Attribute
    {
    }
}
